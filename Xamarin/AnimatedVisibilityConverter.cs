using System;
using System.Globalization;
using Android.Views;
using Android.Views.Animations;
using Java.Lang;
using MvvmCross.Platform.Converters;

namespace WMSMobile.android.Utils
{
    public class AnimatedVisibilityConverter : IMvxValueConverter
    {
        private readonly View view;

        public AnimatedVisibilityConverter(View view)
        {
            this.view = view;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ValidateType(targetType);

            ViewStates viewStateToValue = FindToValue(value);

            if (viewStateToValue == view.Visibility)
                return view.Visibility;

            ApplyFade(viewStateToValue);

            return ViewStates.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private void ApplyFade(ViewStates viewStateToValue)
        {
            if (viewStateToValue == ViewStates.Gone)
                FadeOut();
            else if (viewStateToValue == ViewStates.Visible)
                FadeIn();
        }

        private void FadeIn()
        {
            view.Alpha = 0;
            view.Animate().SetInterpolator(new DecelerateInterpolator()).SetDuration(500).Alpha(1);
        }

        private void FadeOut()
        {
            view.Alpha = 1;
            view.Animate()
                .SetDuration(250)
                .Alpha(0)
                .TranslationYBy(-200)
                .SetInterpolator(new AccelerateInterpolator())
                .WithEndAction(new Runnable(() => { view.Visibility = ViewStates.Gone; }));
        }

        private static ViewStates FindToValue(object value)
        {
            bool toValue = (bool) value;
            ViewStates newViewStateValue = toValue ? ViewStates.Visible : ViewStates.Gone;
            return newViewStateValue;
        }

        private static void ValidateType(Type targetType)
        {
            if (targetType != typeof (ViewStates))
                throw new InvalidOperationException("The target must be ViewStates");
        }
    }
}